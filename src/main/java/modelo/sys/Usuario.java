
package modelo.sys;

/**
 *
 * @author Benitez
 */
//usuarios del sistema: tabla sys_usuario
public class Usuario {
    private int id;
    private String nombre;
    private String user_name;
    private String password;
    private boolean activo;
    private int id_rol;
    private Rol Rol; //asocio el campo id fk a la clase entidad
    private int id_empresa;

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public Rol getRol() {
        return Rol;
    }

    public void setRol(Rol Rol) {
        this.Rol = Rol;
    }

    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }

    public Usuario() {
    }

    
    public Usuario(String nombre, String user_name, String password, boolean activo) {
        this.nombre = "";
        this.user_name = "";
        this.password = "";
        this.activo = false;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    
    
}
