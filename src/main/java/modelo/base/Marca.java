/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo.base;

/**
 *
 * @author Benitez
 */
public class Marca {
     private int id;
     private String descripcion;

    public Marca() {
    }

    public Marca(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }
     
     

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
