
package conexion;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author Benitez
 */
public class Conexion {
    public final String host = "jdbc:mysql://localhost:3306/";
    public String db = "erp_v1";
    public String user = "root";
    public String pwd = "";
    private Connection con;

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public void conectar() throws Exception
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection(host+db,user,pwd);
        }
        catch (Exception e) 
        {
            throw e;
        }
    }
    public void desconectar() throws Exception
    {
        try 
        {
            if(con!=null)
            {
                if (con.isClosed()==false) 
                {
                    con.close();
                }
            }
        } 
        catch (Exception e) 
        {
            throw e;
        }
    }
}
