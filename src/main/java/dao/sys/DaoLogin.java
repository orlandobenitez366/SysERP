/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.sys;

//AppService para login

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.AppConsts;
import modelo.sys.Usuario;

public class DaoLogin extends Conexion{
    
    
    public boolean LogIn(String username, String password) throws Exception{
        try {
            ResultSet res;
            this.conectar();
            String sql="SELECT id FROM sys_usuario WHERE user_name = ? AND PASSWORD = ? AND id_empresa = ? AND activo = 1 ";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1,username);
            pre.setString(2, password);
            pre.setInt(3, AppConsts.idEmpresaActual);
            res=pre.executeQuery();
            return res.next();
        } catch (Exception ex) {
            throw ex;
        }
    }
}
