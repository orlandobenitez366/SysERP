
package dao;

//interfaz para operaciones crud

import java.util.List;

public interface IOperaciones {
    public boolean insertar(Object obj) throws Exception;
    public boolean eliminar(Object obj) throws Exception;
    public boolean modificar(Object obj) throws Exception;
    public List<?> seleccionar() throws Exception; //el signo ? indica que le puedo cambiar el tipo en la clase en la que implemento la interfaz
    
}
