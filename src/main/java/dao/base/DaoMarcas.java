/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.base;

import conexion.Conexion;
import dao.IOperaciones;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.AppConsts;
import modelo.base.Marca;

/**
 *
 * @author Benitez
 */
public class DaoMarcas extends Conexion implements IOperaciones{

    Marca marca = new Marca();
    List<Marca> listaMarcas = new ArrayList<>();
    
    @Override
    public boolean insertar(Object obj) throws Exception {
        int filas = 0;
        try 
        {
            this.conectar();
            marca = (Marca) obj; //casteo el tipo obj a la clase objeto que necesito
            String sql="INSERT INTO bs_marca (descripcion, id_empresa) VALUES (?,?)";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1,marca.getDescripcion());
            pre.setInt(2, AppConsts.idEmpresaActual);
            filas = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
           return false; //si hay error devuelvo false
        }
        finally
        {
            this.desconectar();
            if (filas > 0) {
                return true;
            }else{
                return false;
            }
        }
    }

    @Override
    public boolean eliminar(Object obj) throws Exception {
        try 
        {
            this.conectar();
            marca = (Marca) obj;
            String sql="DELETE FROM bs_marca WHERE id = ?";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setInt(1, marca.getId());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            return false;
        }
        finally
        {
            this.desconectar();
            return true;
        }
    }

    @Override
    public boolean modificar(Object obj) throws Exception {
       int filas = 0;
        try 
        {
            this.conectar();
            marca = (Marca) obj; //casteo el tipo obj a la clase objeto que necesito
            String sql="UPDATE bs_marca SET descripcion = ? WHERE id = ?";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1,marca.getDescripcion());
            pre.setInt(2, marca.getId());
            filas = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
           return false; //si hay error devuelvo false
        }
        finally
        {
            this.desconectar();
            if (filas > 0) {
                return true;
            }else{
                return false;
            }
        }
    }

    @Override
    public List<Marca> seleccionar() throws Exception {
        listaMarcas = new ArrayList<>();
        ResultSet res;
        try 
        {
           this.conectar();
           String sql="SELECT id, descripcion FROM bs_marca WHERE id_empresa = ?";
           PreparedStatement pre=this.getCon().prepareStatement(sql);
           pre.setInt(1, AppConsts.idEmpresaActual);
           res=pre.executeQuery();
            while (res.next()) 
            {
                Marca marca = new Marca();
                marca.setId(res.getInt("id"));
                marca.setDescripcion(res.getString("descripcion"));
                
                listaMarcas.add(marca);
            }
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return listaMarcas;
    }
    
}
