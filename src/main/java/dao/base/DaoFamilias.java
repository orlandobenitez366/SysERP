
package dao.base;

import conexion.Conexion;
import dao.IOperaciones;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.AppConsts;
import modelo.base.Familia;

/**
 *
 * @author Benitez
 */
public class DaoFamilias extends Conexion implements IOperaciones{
    Familia familia = new Familia();
    List<Familia> listaFamilia = new ArrayList<>();
     
    
    @Override
    public boolean insertar(Object obj) throws Exception {
        int filas = 0;
        try 
        {
            this.conectar();
            familia = (Familia) obj; //casteo el tipo obj a la clase objeto que necesito
            String sql="INSERT INTO bs_familia (descripcion, id_empresa) VALUES (?,?)";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1,familia.getDescripcion());
            pre.setInt(2, AppConsts.idEmpresaActual);
            filas = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
           return false; //si hay error devuelvo false
        }
        finally
        {
            this.desconectar();
            if (filas > 0) {
                return true;
            }else{
                return false;
            }
        }
    }

    @Override
    public boolean eliminar(Object obj) throws Exception {
        try 
        {
            this.conectar();
            familia = (Familia) obj;
            String sql="DELETE FROM bs_familia WHERE id = ?";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setInt(1, familia.getId());
            pre.executeUpdate();
        } 
        catch (Exception e) 
        {
            return false;
        }
        finally
        {
            this.desconectar();
            return true;
        }
    }

    @Override
    public boolean modificar(Object obj) throws Exception {
        int filas = 0;
        try 
        {
            this.conectar();
            familia = (Familia) obj;
            String sql="UPDATE bs_familia SET descripcion = ? WHERE id = ?";
            PreparedStatement pre=this.getCon().prepareStatement(sql);
            pre.setString(1,familia.getDescripcion());
            pre.setInt(2, familia.getId());
            filas = pre.executeUpdate();
        } 
        catch (Exception e) 
        {
           return false; //si hay error devuelvo false
        }
        finally
        {
            this.desconectar();
            if (filas > 0) {
                return true;
            }else{
                return false;
            }
        }
    }

    @Override
    public List<Familia> seleccionar() throws Exception {
        listaFamilia = new ArrayList<>();
        ResultSet res;
        try 
        {
           this.conectar();
           String sql="SELECT id, descripcion FROM bs_familia WHERE id_empresa = ?";
           PreparedStatement pre=this.getCon().prepareStatement(sql);
           pre.setInt(1, AppConsts.idEmpresaActual);
           res=pre.executeQuery();
            while (res.next()) 
            {
                Familia familia = new Familia();
                familia.setId(res.getInt("id"));
                familia.setDescripcion(res.getString("descripcion"));
                
                listaFamilia.add(familia);
            }
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return listaFamilia;
    }
    
}
