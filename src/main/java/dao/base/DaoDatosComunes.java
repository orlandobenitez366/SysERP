/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.base;

import conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import modelo.base.Empresa;

/**
 *
 * @author Benitez
 */
//clase para cargar combos y otras cosas comunes
public class DaoDatosComunes extends Conexion{
    
    public ArrayList<Empresa> GetEmpresasComboBoxItems() throws Exception{
        ArrayList listaempresas=new ArrayList();
        ResultSet res;
        try 
        {
           this.conectar();
           String sql="SELECT  id, nombre FROM bs_empresa WHERE activo = 1";
           PreparedStatement pre=this.getCon().prepareStatement(sql);
           res=pre.executeQuery();
            while (res.next()) 
            {
                Empresa emp=new Empresa();
                emp.setId(res.getInt("id"));
                emp.setNombre(res.getString("nombre"));
                listaempresas.add(emp);
            }
        } 
        catch (Exception e) 
        {
            throw e;
        }
        finally
        {
            this.desconectar();
        }
        return listaempresas;
    }
}
